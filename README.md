Cette application est développée avec Angular 7 pour la partie front. Tandis que la partie Backend est montée avec Express/NodeJs Restfull Webservice et que la source de données réside sur Mongo database (cloud Mongodb Atlas). 

Le fonctionnemnt de l'application est comme suit :

 En tapant un mot ou une phrase à rechercher dans la barre de recherche et taper sur entrer ou sur la loupe pour lancer la recherche . En même temps un temporisateur se lance pour notifier l'utilisateur que la requête est en attente de resultat depuis le backend.

Un filter de selection de date est disponible à droite de la barre de recherche, il s'active pour filtrer les résultats de la recherche.

Pour modifier localement les lignes sur la table résultat, il suffit de cliquer sur cette ligne et une pop-up s'ouvre pour apporter les modifications souhaitées.

Les étapes à suivre pour lancer l'application :

1. git clone https://gitlab.com/ISMAILHD/canal-plus-app.git
2. cd canal-plus-app
3. npm install
4. npm install concurrently --save
5. npm start
6. aller dans un navigateur et tapez http://localhost:4200/

Le lancement de npm start permet de lancer localement le front et le backend à la fois sur les ports 4200 et 3000.