import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFilter'
})
export class DateFilterPipe implements PipeTransform {

  transform(items: any[], year: string): any[] {
    let _year = parseInt(year, 10);
    if (!items) return [];
    if(year === null || year.length !== 4) return items;
    return items.filter(it => {
      return it["startYear"] === _year;
    });
  }

}
