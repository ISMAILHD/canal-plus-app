import { Component, Input, ViewChild } from '@angular/core';
import { MovieService } from "../movie.service";
import { Movie } from '../movie';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { UpdateDialogComponent } from "../update-dialog/update-dialog.component";

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})

export class MoviesListComponent {

  displayedColumns: string[] = ['primaryTitle', 'originalTitle', 'startYear', 'genres'];
  years: number[];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
  }

  dataSource: any;
  noresult: boolean;
  spinner: boolean;
  movies: Movie[];

  @Input()
  start_year_valid: boolean;

  @Input()
  searchText: string;

  @Input()
  year: string;

  constructor(private movieService: MovieService, public dialog: MatDialog) {
    this.searchText = '';
    this.year = null;
    this.noresult = false;
    this.spinner = false;
    this.start_year_valid = false;
  }

  changed() {
    this.noresult = false;
    this.spinner = true;
    if (this.searchText) {
      this.movieService.getMovies(this.searchText).subscribe(
        movies => {
          this.year = null;
          this.years = [];
          movies.map(m => {
            m["startYear"]>999 && !this.years.includes(m["startYear"]) && this.years.push(m["startYear"])
          })
          this.spinner = false;
          this.noresult = (movies.length !== 0);
          this.movies = movies;
          this.dataSource = new MatTableDataSource<Movie>(movies);
          setTimeout(() => {
            this.years.sort((a,b)=> b-a);
            this.dataSource.paginator = this.paginator;
            this.years.unshift(null)
          }, 0);
        }
      )
    } else this.spinner = false;
  }

  displayrecord(ev) {
    this.dialog.open(UpdateDialogComponent, {
      data: ev
    });
  }

  closemd() {
    this.dialog.closeAll()
  }

  applyFilter(filterValue: string) {
    if(this.searchText && this.searchText.length !== 0) {
      this.dataSource.filter = filterValue;
    }
  }

}