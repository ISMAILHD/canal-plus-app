import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Movie } from "./movie";

const api_uri = 'http://localhost:3000/movies/';
@Injectable({
  providedIn: 'root'
})
export class MovieService {
  constructor(private _http: HttpClient) { }

  getMovies(query: string): Observable<Movie[]>{
    let uri = api_uri + query;
    return this._http.get<Movie[]>(uri)
  }

  updateMovie(movie: Movie): void {
    // this.update<Movie>()
  }
}
