import { Component, OnInit, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Movie } from "../movie";
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-update-dialog',
  templateUrl: './update-dialog.component.html',
  styleUrls: ['./update-dialog.component.css']
})
export class UpdateDialogComponent implements OnInit {
  @Input()
  movie: Movie;
  selctions;
  constructor(@Inject(MAT_DIALOG_DATA) public data: Movie, private movieService: MovieService, private dialogRef: MatDialogRef<UpdateDialogComponent>) {
    this.movie = this.data;
    this.selctions = ['Animation','Comedy','Romance','short'];
  }

  ngOnInit() {
  }

  updateMovie(){
    this.dialogRef.close();
  } 
  
  close(){
    this.dialogRef.close();
  }
}