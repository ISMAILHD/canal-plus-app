import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from "./material/material.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from './app.component';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { DateFilterPipe } from './date-filter.pipe';
import { UpdateDialogComponent } from './update-dialog/update-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviesListComponent,
    DateFilterPipe,
    UpdateDialogComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule
  ],
  entryComponents: [
    UpdateDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
