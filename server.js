const Express = require("express");
const BodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;

const CONNECTION_URL = "mongodb+srv://user_1234:user_1234@mediahubcluster-yuve1.mongodb.net/test?retryWrites=true";
const DATABASE_NAME = "mediahubdb";

var app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

var database, collection;

app.listen(3000, () => {
    MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(DATABASE_NAME);
        collection = database.collection("mediahubcollection");
        console.log("back Connected to database `" + DATABASE_NAME + "`! on port : "+3000);
    });
});

app.get("/movies/:searchword", (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    const searchString = request.params.searchword;
    collection.find({ '$or': [{ 'primaryTitle': { '$regex': searchString, '$options': 'i' } },{ 'originalTitle': { '$regex': searchString, '$options': 'i' } }] }).toArray(function(err, result) {
        if (err) throw err;
        response.send(result);
      });
});
